<?php 

	include 'conexion.php';

	$error = "";

	// actualizamos
	if(isset($_GET["boton"])){
		// para actualizar =====================
		$sql = "UPDATE `pokemons` SET `nombre` = '". $_GET["name"] ."', `altura` = '". $_GET["altura"] ."', `peso` = '". $_GET["peso"] ."', `energiaBase` = '". $_GET["energia"] ."' WHERE `pokemons`.`idPokemon` = ". $_GET["id"] ."; ";

		//id=47&name=asdasdas&altura=123234&peso=234234&energia=234234&boton=Modificar

		$res = $db->query($sql);

		header("Location: ./makepoke.php");

	}


	// Mostramos en los input
	if(isset($_GET["id"])){
		
		$sql = "SELECT * FROM `pokemons` WHERE `idPokemon` = ". $_GET["id"];

		$res = $db->query($sql);

		$fila = $res->fetch_array();


	}else{
		$error="No hay id"; 
	}




 ?>


 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">


 	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Nerko+One&family=Noto+Sans:wght@400;700&display=swap" rel="stylesheet">  


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">



 	<link rel="stylesheet" href="estilo.css">

 	<title>MakePoke</title>
 </head>
 <body>

 	<!-- Encabezado -->
 	<header>
 			<div class="titulo-web">
 			POKE<span>DB</span>
 			</div>
 	</header>

 	<div class="nav">
		<a href="makepoke.php" class="nav-btn_volver"><i class="fas fa-undo btn btn-orange"></i></a>
	</div>

	<!-- Contenido -->
 	<div id="contenido">

	 	<!-- Columna 1 -->
 		<div class="formulario">
 			<h1>Editar Pokemon!</h1>
			<form action="" method="GET">
		<input type="hidden" name="id" value="<?php echo $fila["idPokemon"]; ?>">

		<input type="text" name="name"  value="<?php echo $fila["nombre"]; ?>">
		<input type="number" name="altura" placeholder="altura" value="<?php echo $fila["altura"]; ?>">
		<input type="number" name="peso" placeholder="peso" value="<?php echo $fila["peso"]; ?>">
		<input type="number" name="energia" placeholder="energia" value="<?php echo $fila["energiaBase"]; ?>">
		
		<button type="submit" name="boton" value="modificar" class="btn-next">
				<i class="far fa-edit"></i>Aceptar</button>
	</form>

			<div class="error">
				<?php
					echo $error; 
				 ?>
			</div>

 		</div>

 	</div>
	
	
 	
 	<footer>
 		<div class="pie">
 			MattProfe @ 2020
 		</div>
 	</footer>
 	
 </body>
 </html>