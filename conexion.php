<?php 
	
	// Constantes
	define("HOST","localhost"); // Servidor
	define("USER","poke");	// Usuario DB
	define("PASS","poke2020");	// Pass
	define("DB","poke");	// Nombre DB

	// Conexión con la db
	$db = new mysqli(HOST, USER, PASS, DB);

	// Revisamos si en la base existe la tabla
	$res = $db->query("SHOW TABLES LIKE 'pokemons'");
			
	// Si la tabla no existe entonces la creamos
	if($res->num_rows == 0){
		$res = $db->query("CREATE TABLE `pokemons` (
						  `idPokemon` int(11) NOT NULL,
						  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
						  `altura` double NOT NULL,
						  `peso` double NOT NULL,
						  `energiaBase` int(11) NOT NULL,
						  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$res = $db->query("ALTER TABLE `pokemons` ADD PRIMARY KEY (`idPokemon`);");

		$res = $db->query("ALTER TABLE `pokemons` MODIFY `idPokemon` int(11) NOT NULL AUTO_INCREMENT;");
	}


 ?>