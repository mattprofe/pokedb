<?php 
	include 'conexion.php';

	$error = "";
	
	// Si se presiono agregar
	if(isset($_GET["boton"])){

		$name = $_GET["name"];
		$peso = $_GET["peso"];
		$altu = $_GET["altura"];
		$ener = $_GET["energia"];

		// Agregar un nuevo pokemon
		$sql = "INSERT INTO `pokemons` (`idPokemon`, `nombre`, `altura`, `peso`, `energiaBase`, `fecha`) 

		VALUES (NULL,
				 '" . $name . "',
				  '". $peso ."', 
				  '". $altu ."',
				   '". $ener ."', 
				   current_timestamp()); ";

		// retorna un objeto de consulta
		$res = $db->query($sql);

		// Mensaje
		$error= "Se agrego un registro.";
	}


	// Lista los pokemons
	$sql = "SELECT * FROM `pokemons`";

	//retorna un objeto de consulta
	$res = $db->query($sql);

	// Acumulador de filas
	$listado = "";

	//retorna un objeto fila
	while($fila = $res->fetch_array()){
		$listado .= '<div class="fila">
						</span>
						<span class="borrar">
							<a href="borrar.php?id='.$fila["idPokemon"].'"><i class="far fa-trash-alt"></i></a>
						</span>
						<span class="editar"><a href="modi.php?id='.$fila["idPokemon"] .'"><i class="far fa-edit"></i></a></span>
						<span class="nombre">'.$fila["nombre"].'</span>
						<span class="energia">'.$fila["energiaBase"].'
						</div>';
	}

	
 ?>

  <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">


 	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Nerko+One&family=Noto+Sans:wght@400;700&display=swap" rel="stylesheet">  

	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">


 	<link rel="stylesheet" href="estilo.css">

 	<title>MakePoke</title>
 </head>
 <body>

 	<!-- Encabezado -->
 	<header>
 			<div class="titulo-web">
 			POKE<span>DB</span>
 			</div>
 	</header>

	<!-- Contenido -->
 	<div id="contenido">

	 	<!-- Columna 1 -->
 		<div class="formulario">
 			<h1>Nuevo Pokemon!</h1>
			<form action="" method="GET" >
				<input type="text" name="name" placeholder="Nombre" autofocus="">
				<input type="number" name="altura" placeholder="Altura">
				<input type="number" name="peso" placeholder="Peso">
				<input type="number" name="energia" placeholder="Energia">
			
				<button type="submit" name="boton" value="agregar" class="btn-next">
				<i class="fas fa-plus-circle"></i>Agregar</button>
			</form>

			<div class="error">
				<?php
					echo $error; 
				 ?>
			</div>

 		</div>

		<!-- Columna 2 -->
 		<div class="listado">
 			<h1>Listado de Pokemons</h1>

 			<!-- encabezados de la lista -->
 			<div class="titulos">
 				<div class="borrar">Borrar</div>
 				<div class="editar">Editar</div>
 				<div class="nombre">Nombre</div>
 				<div class="enegia">Energía</div>
 				
 			</div>

 			<!-- Filas de pokemons -->
 			<?php 
 				echo $listado;
 			 ?>

 		</div>
 	</div>

 	
 	<footer>
 		<div class="pie">
 			MattProfe @ 2020
 		</div>
 	</footer>
 	
 </body>
 </html>