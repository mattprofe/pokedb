pokedb
=========
Es la actividad de final del periodo 2 de la materia de Laboratorio de sistemas operativos de 6 año de informática. 
Se utiliza para su desarollo y pruebas:

- HTML.
- CSS.
- PHP.
- MySql(MariaDB).

Temas que se aplican
====================

- Manejo correcto de etiquetas HTML5.
- Uso de reglas CSS3 e iconos de Fontawesome.
- Lógica de programación en PHP y buenas prácticas.
- Creación de bases de datos relacionales en MySql.
- Comprensión y aplicación de consultas sql (SELECT, INSERT, UPDATE y DELETE).
- Diseño de formularios con conexión a bases de datos.

Autor
=====
- Matias Baez
- @matt_profe
- mbcorp.matias@gmail.com
- http://www.mbcorp.com.ar
